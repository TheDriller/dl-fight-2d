﻿using UnityEngine;
using System.Collections;
using System.Threading;


public enum AttackTypeEnum
{
	SWORD,
	AXE
}

public abstract class DLAttackType 
{
	public DLAttackType(int dmg, float dly, float immob)
	{
		Damage = dmg;
		Delay = dly;
		Immobilize = immob;
	}
	public int Damage;
	public float Delay;
	public float Immobilize;
}

public class AxeAttack : DLAttackType
{
	public AxeAttack(int dmg, float dly, float immob)
		: base(dmg, dly, immob)
	{

	}
}

public class SwordAttack : DLAttackType
{
	public SwordAttack(int dmg, float dly, float immob)
		: base(dmg, dly, immob)
	{

	}
}


public class BaronStats : MonoBehaviour {

	public int MaxHealth = 100;
	public int CurrentHealth = 100;

	public int axeDamage = 60;
	float axeDelay;
	float axeImmobilize;

	public int swordDamage = 20;
	float swordDelay;
	float swordImmobilize;

    public float DamageModifier = 1f;
	bool isDamageModified = false;
    public float hitRecoil = 0.8f;
	Mutex damageModifierMutex; 

    public AnimationClip sword1Anim;
    public AnimationClip axeAnim;
    public AnimationClip hitAnim;

	public DLAttackType[] AttackTypeList;

	//Components
    Reactions reactions;
    Animator anim;
    BaronCombat baronCombat;
    Movement baronMov;
    
	void Start ()
	{
        reactions = GetComponent<Reactions>();
		CurrentHealth = MaxHealth;
        anim = GetComponentInChildren<Animator>();

        swordDelay = sword1Anim.length;
        axeDelay = axeAnim.length;

        swordImmobilize = sword1Anim.length;
        axeImmobilize = axeAnim.length;

        hitRecoil = hitAnim.length;

		damageModifierMutex = new Mutex();

        AttackTypeList = new DLAttackType[2];
		AttackTypeList[0] = new SwordAttack(swordDamage, swordDelay, swordImmobilize);
		AttackTypeList[1] = new AxeAttack(axeDamage, axeDelay, axeImmobilize);

        baronCombat = GetComponent<BaronCombat>();
        baronMov = GetComponent<Movement>();
    }
	
	void Update () 
	{
	
	}

	public void HitCallback(int incommingDamage, float xHitPosition)
	{
        if (!BlocksHit(xHitPosition))
        {
            ModifiyHealth(-incommingDamage);
        }
    }

    public void ResetHealth() {
		CurrentHealth = MaxHealth;
    }

	public DLAttackType GetAttackType(AttackTypeEnum enumType)
	{
		DLAttackType type = null;
		switch (enumType)
		{
			case AttackTypeEnum.SWORD:
				type = AttackTypeList[0];
				break;
			case AttackTypeEnum.AXE:
				type = AttackTypeList[1];
				break;
			default:
				Debug.LogAssertion("Missing attack type");
				break;
		}
		return type;
	}
    
    public bool BlocksHit(float xHitPosition)
    {
        if (baronCombat.IsBlocking())
        {
            if (xHitPosition - transform.position.x > 0 && !baronMov.facingRight || xHitPosition - transform.position.x < 0 && baronMov.facingRight)
                return false;
            else {
                return true;
            }
        }
        else {
            return false;
        }
    }

    public void ModifiyHealth(int amount)
    {
        if (CurrentHealth + amount <= 0)
        {
            CurrentHealth = 0;
            reactions.Die();            
        }
        else if (CurrentHealth + amount > MaxHealth)
        {
            CurrentHealth = MaxHealth;
        }
        else if(amount < 0 )
        {
            anim.SetTrigger("Hit");
            CurrentHealth += amount;
        }
        Debug.Log("Damage modified: " + amount + " to " + gameObject.name);
    }

	public void ModifyDamage(float multiplier, float time)
	{
		damageModifierMutex.WaitOne();
		if (!isDamageModified)
		{
			StartCoroutine(IEModifyDamage(multiplier, time));	
		}
		damageModifierMutex.ReleaseMutex();
	}

	private IEnumerator IEModifyDamage(float multiplier, float time)
	{
		damageModifierMutex.WaitOne();
		isDamageModified = true;
		damageModifierMutex.ReleaseMutex();

		float previousDmgModifier = DamageModifier;
		DamageModifier = multiplier;
		yield return new WaitForSeconds(time);
		DamageModifier = previousDmgModifier;

		damageModifierMutex.WaitOne();
		isDamageModified = false;
		damageModifierMutex.ReleaseMutex();
	}
}
