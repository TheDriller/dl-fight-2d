﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour {


	public GameObject Baron;
	private BaronStats baronStats;
	private Slider slider;

	void Start()
	{
		baronStats = Baron.GetComponent<BaronStats>();
		slider = GetComponent<Slider>();
		slider.maxValue = baronStats.MaxHealth;
	}

	// Update is called once per frame
	void Update () 
	{
		slider.value = baronStats.CurrentHealth;
	}
}
