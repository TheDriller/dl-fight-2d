﻿using UnityEngine;
using System.Collections;

public class AnimatorLinker : MonoBehaviour {

    BaronCombat baronCombat;
    Movement baronMovement;
    
	void Start () 
	{
        baronCombat = GetComponentInParent<BaronCombat>();
        baronMovement = GetComponentInParent<Movement>();
    }

	public void StartSlash()
	{
		baronMovement.ImmobilizeCallback(AttackTypeEnum.SWORD);
	}

	public void StartAxe()
	{
		baronMovement.ImmobilizeCallback(AttackTypeEnum.AXE);
	}

	public void onSlash()
	{
        baronCombat.AttackCallback(AttackTypeEnum.SWORD);
	}

	public void onAxe()
	{
        baronCombat.AttackCallback(AttackTypeEnum.AXE);
	}

	public void OnHit()
	{
		baronMovement.OnHitCallback();
	}

    public void OnBlock(int blocking)
    {
        baronCombat.SetBlocking(blocking == 0 ? false : true);
    }
}
