﻿using UnityEngine;
using System.Collections;

public abstract class PowerUpBase : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public abstract void ApplyEffect(BaronStats baron);
}
