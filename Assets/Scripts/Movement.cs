﻿using UnityEngine;
using System.Collections;


 
public class Movement: MonoBehaviour {

    [HideInInspector]
    public bool facingRight = false;
    [HideInInspector]
    public bool grounded = false;
    float groundRadius = 0.1f;

	//Components
	private Rigidbody2D rigidb;
	private Animator anim;
    public Transform groundCheck1;
    public Transform groundCheck2;
    public LayerMask whatIsGround;
	private BaronSetup baronSetup;
	private BaronStats baronStats;
    private BaronCombat baronCombat;

    public float jumpForce;
    public float maxSpeed;


    public bool immobilized = false;
    
    void Start()
    {
        rigidb = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();
		baronSetup = GetComponent<BaronSetup>();
		baronStats = GetComponent<BaronStats>();
        baronCombat = GetComponent<BaronCombat>();


        Debug.Assert(baronSetup != null, "No Baron Setup component");
    }
    
    void Update()
    {
        anim.SetFloat("xSpeed", Mathf.Abs(rigidb.velocity.x));
        if (grounded)
        {
			if(Input.GetButtonDown(baronSetup.DLInput.Jump) && !baronCombat.IsBlocking())
			{
				rigidb.AddForce(new Vector2(0, jumpForce));
                anim.SetTrigger("Jump");
			}
			anim.speed = 1;
        }
		else
		{
			//anim.speed = 0.4f;
		}

    }

    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck1.position, groundRadius, whatIsGround) || Physics2D.OverlapCircle(groundCheck2.position, groundRadius, whatIsGround);
        anim.SetBool("Grounded", grounded);

		float move = 0f;

        if (immobilized && grounded || baronCombat.IsBlocking())
        {
            move = 0f;
            rigidb.velocity = new Vector2(move * maxSpeed, rigidb.velocity.y);
        }
        else if (immobilized && !grounded)
        {
            
        }
        else
        {
			move = Input.GetAxis(baronSetup.DLInput.Horizontal);
			//If not input is detected, use keys
			if (move == 0)
			{
				move = Input.GetAxis(baronSetup.DLInput.Horizontal + "_Alt");
			}	
            rigidb.velocity = new Vector2(move * maxSpeed, rigidb.velocity.y);
        }
        
        if (move > 0 && !facingRight)
        {
            Flip();
        }
        else if (move < 0 && facingRight)
        {
            Flip();
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

	public void ImmobilizeCallback(AttackTypeEnum attackType)
	{
		StartCoroutine(Immobilize(baronStats.GetAttackType(attackType).Immobilize));
	}

    public IEnumerator Immobilize(float time)
    {
        immobilized = true;
        GetComponent<BaronCombat>().enabled = false;
        yield return new WaitForSeconds(time);
        immobilized = false;
        GetComponent<BaronCombat>().enabled = true;
    }

	public void OnHitCallback()
	{
		StartCoroutine(Immobilize(baronStats.hitRecoil));
	}

	void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "PowerUp")
        {
            PowerUpBase powerUp = other.gameObject.GetComponent<PowerUpBase>();
            if (powerUp != null)
            {
                powerUp.ApplyEffect(baronStats);
				//Todo CHANGGGE for pool
				powerUp.transform.position = new Vector3(0, 0, 0);
				Destroy(other.gameObject);
            }
            else
            {
                Debug.Log("Unexistant PowerUp");
            }
        }
    }
}