﻿using UnityEngine;
using System.Collections;

public class DeathZone : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<Reactions>().Die();
        }
        else if(other.tag == "PowerUp")
        {
            // TODO : pool
            GameObject.Destroy(other.gameObject);
        }
    }

}
