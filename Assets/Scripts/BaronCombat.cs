﻿using UnityEngine;
using System.Collections;

public class BaronCombat: MonoBehaviour {



	//Components
    private Animator anim;
	private BaronSetup baronSetup;
	private BaronStats baronStats;
    private Movement baronMov;

	//Attack properties
	public float attackCooldown = 0.5f;
	public float attackRadius = 4;
	public float attackOffset = 1;
	public bool attackDebug = false;

	private float deltaTime;
	private LayerMask attackLayerMask;

    private bool blocking = false;

	// Use this for initialization
	void Start () 
	{
        anim = GetComponentInChildren<Animator>();
		baronSetup = GetComponent<BaronSetup>();
		Debug.Assert(baronSetup != null, "No Baron Setup component");
		baronStats = GetComponent<BaronStats>();
        baronMov = GetComponent<Movement>();
        attackLayerMask = 1 << LayerMask.NameToLayer("Player1");
        attackLayerMask |= 1 << LayerMask.NameToLayer("Player2");
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!IsBlocking())
        {
            if (Input.GetButtonDown(baronSetup.DLInput.Fire1) && Time.time - deltaTime > attackCooldown)
            {
                anim.SetTrigger("Slash1");
                deltaTime = Time.time;
                attackCooldown = baronStats.GetAttackType(AttackTypeEnum.SWORD).Immobilize;
            }
            if (Input.GetButtonDown(baronSetup.DLInput.Fire2) && Time.time - deltaTime > attackCooldown)
            {
                anim.SetTrigger("Axe");
                deltaTime = Time.time;
                attackCooldown = baronStats.GetAttackType(AttackTypeEnum.AXE).Immobilize;
            }
        }
        if (Input.GetButton(baronSetup.DLInput.Block) && baronMov.grounded)
        {
            anim.SetBool("Blocking", true);
        }
        else
        {
            anim.SetBool("Blocking", false);
        }
	}

	public void AttackCallback(AttackTypeEnum attackType)
	{
		Collider2D[] colliders = Physics2D.OverlapCircleAll(new Vector3(attackOffset * -transform.localScale.x, 0, 0) + transform.position, attackRadius, attackLayerMask);
		foreach (var collider in colliders)
		{
			if (collider.attachedRigidbody.tag == "Player" && collider.gameObject != gameObject)
			{
                DLAttackType type = baronStats.GetAttackType(attackType);
                collider.GetComponent<BaronStats>().HitCallback((int)(type.Damage * baronStats.DamageModifier), transform.position.x);
			}
		}
	}

	void OnDrawGizmos()
	{
		if (attackDebug)
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere(new Vector3(attackOffset * -transform.localScale.x, 0, 0) + transform.position, attackRadius);
		}
	}

    public bool IsBlocking()
    {
        return blocking;
    }

    public void SetBlocking(bool block)
    {
        blocking = block;
    }

    public void ResetCombat()
    {
        blocking = false;
    }
}