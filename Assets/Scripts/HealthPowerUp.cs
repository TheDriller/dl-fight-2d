﻿using UnityEngine;
using System.Collections;

public class HealthPowerUp : PowerUpBase {

    public int amountRestored = 100;

    public override void ApplyEffect(BaronStats baronStats) 
	{
		baronStats.ModifiyHealth(amountRestored);		
    }

}
