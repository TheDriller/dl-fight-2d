﻿using UnityEngine;
using System.Collections;

public class Reactions : MonoBehaviour {

    Vector3 spawnpoint;

    Animator anim;
    BaronStats stats;

    int aliveLayer;
    public float respawnTime = 2;

    Rigidbody2D rigidb2D;
    BaronCombat baronCombat;

	void Start ()
    {
        spawnpoint = GameObject.FindGameObjectWithTag("Respawn").transform.position;
        anim = GetComponentInChildren<Animator>();
        stats = GetComponent<BaronStats>();
        aliveLayer = gameObject.layer;
        rigidb2D = GetComponent<Rigidbody2D>();
        baronCombat = GetComponent<BaronCombat>();
    }

    public void Die()
    {
        rigidb2D.velocity = Vector3.zero;
        anim.SetTrigger("Die");
        GetComponent<Movement>().enabled = false;
        GetComponent<BaronCombat>().enabled = false;
        gameObject.layer = LayerMask.NameToLayer("Dead");
        StartCoroutine(SpawnTime());
    }

    public void Spawn()
    {
        anim.Rebind();
        
        GetComponent<Movement>().enabled = true;
        GetComponent<BaronCombat>().enabled = true;
        baronCombat.ResetCombat();
        GetComponent<Collider2D>().enabled = true;
        transform.position = spawnpoint;
        stats.ResetHealth();
        gameObject.layer = aliveLayer;
    }

    IEnumerator SpawnTime() {
        yield return new WaitForSeconds(respawnTime);
        Spawn();
    }
}
