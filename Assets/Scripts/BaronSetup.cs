﻿using UnityEngine;
using System.Collections;

public struct DLInputManager
{
	public DLInputManager(string prefix)
	{
		Fire1 = prefix + "_Fire1";
		Fire2 = prefix + "_Fire2";
		Horizontal = prefix + "_Horizontal";
		Jump = prefix + "_Jump";
        Block = prefix + "_Block";
    }
	public string Fire1;
	public string Fire2;
	public string Horizontal;
	public string Jump;
    public string Block;
}

public class BaronSetup : MonoBehaviour {

	private string PlayerName;
	public int PlayerNumber;
	[HideInInspector]
	public DLInputManager DLInput;

	// Use this for initialization
	void Awake () 
	{
		PlayerName = "Player" + PlayerNumber;
		Debug.Assert(PlayerNumber != 1 || PlayerNumber != 2, "Invalid Player Number: " + PlayerNumber);
		DLInput = new DLInputManager(PlayerName);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
