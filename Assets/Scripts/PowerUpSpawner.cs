﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerUpSpawner : MonoBehaviour {

	public float averageTime;
	public float randomTimeRange;
	public float startPositionX;
	public float endPositionX;
	public float spawnHeight;
	public List<GameObject> spawnablePowerUpList;
	private float cooldownRemaining;
	

	// Update is called once per frame
	void Update () {
		cooldownRemaining -= Time.deltaTime;

		if(cooldownRemaining <= 0 )
		{
			//Todo change for pooling
			int powerUpType = Random.Range(0, spawnablePowerUpList.Count);
			GameObject powerUp = (GameObject)GameObject.Instantiate(spawnablePowerUpList[powerUpType], new Vector3(Random.Range(startPositionX, endPositionX), spawnHeight, 0), Quaternion.identity);
			cooldownRemaining = averageTime + Random.Range(-randomTimeRange, randomTimeRange);
			Rigidbody2D powerUpRB2D = powerUp.GetComponent<Rigidbody2D>();
			if (powerUpRB2D != null)
			{
				powerUpRB2D.AddTorque(Random.Range(-100, 100));
			}
		}
	}
}
