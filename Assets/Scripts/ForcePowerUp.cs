﻿using UnityEngine;
using System.Collections;

public class ForcePowerUp : PowerUpBase {

    public float ForceMultiplier = 2f;
    public float Duration = 10;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()    
    {
	
	}

    public override void ApplyEffect(BaronStats baron)
    {
		baron.ModifyDamage(ForceMultiplier, Duration);
    }
}
