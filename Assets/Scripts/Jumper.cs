﻿using UnityEngine;
using System.Collections;
 
public class Jumper: MonoBehaviour {

    public float jumpForce = 500f;
    
    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            other.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
        }
    }
}